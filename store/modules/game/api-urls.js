const BASE_URL = '/v1'

export default {
    DO_FIRST_LIST: `${BASE_URL}/game-data/init`, //get
    DO_ADD_GOLD: `${BASE_URL}/gold/add`, //put
    DO_GENERAL_CHOOSE: `${BASE_URL}/choose-treasure/general`, //post
    DO_HIGH_QUALITY_CHOOSE: `${BASE_URL}/choose-treasure/high-quality`, //post,
    DO_GAME_RESET: `${BASE_URL}/game-data/reset`
}
