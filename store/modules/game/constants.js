export default {
    DO_FIRST_LIST: 'game/doFirstList',
    DO_ADD_GOLD: 'game/doAddCold',
    DO_GENERAL_CHOOSE: 'game/doGeneralChoose',
    DO_HIGH_QUALITY_CHOOSE: 'game/doHighQualityChoose',
    DO_GAME_RESET: 'game/doGameReset'
}
