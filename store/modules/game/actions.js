import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FIRST_LIST]: (store) => {
        return axios.get(apiUrls.DO_FIRST_LIST)
    },
    [Constants.DO_ADD_GOLD]: (store) => {
        return axios.put(apiUrls.DO_ADD_GOLD)
    },
    [Constants.DO_GENERAL_CHOOSE]: (store) => {
        return axios.post(apiUrls.DO_GENERAL_CHOOSE)
    },
    [Constants.DO_HIGH_QUALITY_CHOOSE]: (store) => {
        return axios.post(apiUrls.DO_HIGH_QUALITY_CHOOSE)
    },
    [Constants.DO_GAME_RESET]: (store) => {
        return axios.delete(apiUrls.DO_GAME_RESET)
    },
}
